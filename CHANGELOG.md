## Unreleased



## 0.0.6 (2017-02-11)

 - Only change Chronic.time_class when Rails is used ([#3](https://github.com/zedtux/cucumber-timecop/pull/3)) [[edejong](https://github.com/edejong)]

## 0.0.5 (2014-10-04)

 - Handle cucumber-timecop within or out of a Rails project

## 0.0.4 (2014-08-29)

 - Fixed Time zone error ([#1](https://github.com/zedtux/cucumber-timecop/pull/1)) [[v0dro](https://github.com/v0dro)]

## 0.0.3 (2013-12-01)

 - Remove cucumber.rb file as this gem can't be automatically loaded

## 0.0.2 (2013-12-01)

 - Fixed gem loading and set Chronic time zone to the Ruby Time one

## 0.0.1 (2013-11-23)

 - Initial release
