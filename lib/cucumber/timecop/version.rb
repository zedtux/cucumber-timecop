module Cucumber
  #
  # Cucumber::Timecop::VERSION used to manage the gem version
  #
  # @author [zedtux]
  #
  module Timecop
    VERSION = '0.0.6'
  end
end
